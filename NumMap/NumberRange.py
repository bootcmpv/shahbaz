import numpy as np


class NumberRange:
    # class constructor which takes three parameters: lower bound, upper bound, and the size of the array
    def __init__(self, low, high, size):
        self.low = low
        self.high = high
        self.size = size

    # class method to initialize a real number numpy array
    def real_num(self):
        return np.linspace(self.low, self.high, self.size)

    # class method to initialize a real number skewed array
    # We need to create a numpy skewed array of 50% of density 2 and 50% of density 5
    # So, considering the range 1 - 10 and taking a unit as 1
    # 2 units will make density 2, hence range is low - high/5 (2 units)
    # 5 units will make density 5, hence range is high/2 - high (5 units)
    def skewed_real_num(self):
        skew2 = np.linspace(self.low, self.high / 5, int(self.size / 2))
        skew5 = np.linspace(self.high / 2, self.high, int(self.size / 2))

        return np.append(skew2, skew5)

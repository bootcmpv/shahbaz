class Utility:
    # Empty class constructor
    def __init__(self):
        pass

    # This is the third method to be called
    # This method returns the closest min max points of a point from the skewed array passed to it
    def map_point(self, point, skewed_array):
        # Iterating every point of the skewed array
        for count in range(len(skewed_array)):
            # Checking if the point of the skewed array is greater than the point being considered
            # Or, if it the last point in the skewed array (special condition)
            if skewed_array[count] > point or count == len(skewed_array) - 1:
                # Checking if the point of the skewed array before is equal to the point being considered
                # If yes, go back one more time and return the point with the point greater than it
                if skewed_array[count - 1] == point and count != 1:
                    return skewed_array[count - 2], skewed_array[count]
                # If no, return the previous point with the point greater than it
                else:
                    return skewed_array[count - 1], skewed_array[count]

    # This is the second method to be called
    # This method maps the closest min max points of a point in a tuple
    # It calls the map_point method to find the closest min max points
    def map_tuple(self, point, skewed_array):
        return point, self.map_point(point, skewed_array)

    # This is the first method to be called
    # Creates a dictionary of every point of a numpy array to it's closest min max points from the skewed array
    # It calls the map_tuple method to find the closest min max points
    def points_dict(self, line_array, skewed_array):
        p_dict = {}

        for p in line_array:
            p_dict[p] = self.map_tuple(p, skewed_array)[1]

        return p_dict

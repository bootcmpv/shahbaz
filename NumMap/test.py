from NumberRange import NumberRange
from Utility import Utility


def main():
    low = int(input("Enter lower range: "))
    high = int(input("Enter upper range: "))
    div = int(input("Enter the size of the array: "))
    # Creating NumberRange object
    n_range = NumberRange(low, high, div)
    # Creating Utility object
    util = Utility()

    print(str(div) + " numbers between " + str(low) + " and " + str(high) + " are:")
    # Display the real number numpy array
    print(n_range.real_num())
    print("\nThe skewed array is: ")
    # Display the skewed array
    print(n_range.skewed_real_num())

    print("\nThe points dictionary is as follows: ")
    # Display the final dictionary with each point in the numpy array with it's min max points from the skewed array
    print(util.points_dict(n_range.real_num(), n_range.skewed_real_num()))
    return 0


if __name__ == "__main__":
    main()

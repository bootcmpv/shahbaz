#!../python/bin/python
import os
import pandas as pd
import csv


class Util:
    def __init__(self):
        pass

    def get_categories(self):
        categories = []
        os.chdir("../database")
        for file in os.listdir(os.path.abspath(".")):
            if os.path.isfile(os.path.join(os.getcwd(), file)):
                categories.append(file[:-4])
        return {"categories": categories}

    def get_products(self, category, params):
        params = params.to_dict()
        data = pd.read_csv("../database/" + category + ".csv")

        if "rating" in params:
            data = data.loc[data['rating'] >= float(params["rating"])]
            params.pop("rating")
        if "max_price" in params:
            data = data.loc[data['list_price'] <= float(params["max_price"])]
            params.pop("max_price")
        if "min_price" in params:
            data = data.loc[data['list_price'] >= float(params["min_price"])]
            params.pop("min_price")
        for attrib in params:
            data = data.loc[data[attrib] == params[attrib]]

        return data.set_index('product_id').T.to_dict(orient="dict")

    def add_products(self, category, product_id, product_type, brand, date_of_post, cost_price, list_price, rating, availability):
        with open("../database/" + category + ".csv", 'a') as data:
            writer = csv.writer(data)
            writer.writerow([product_id, product_type, brand, date_of_post, cost_price, list_price, rating, availability])
        data.close()

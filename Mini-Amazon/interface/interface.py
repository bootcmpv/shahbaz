#!../python/bin/python
from flask import Flask, jsonify, abort, request
from utility.util import Util

app = Flask(__name__)
util = Util()


@app.route('/', methods=['GET'])
def index():
    return jsonify({"about": "Welcome To Amazon!"})


@app.route('/kidswear/', methods=['GET'])
def kidswear():
    return jsonify({"about": "Welcome To KidsWear!"})


@app.route('/categories/', methods=['GET'])
def categories():
    if not util.get_categories():
        abort(404)
    else:
        return jsonify(util.get_categories())


@app.route('/menswear/', methods=['GET', 'POST'])
def menswear():
    if request.method == "POST":
        req_data = request.get_json()
        product_id = req_data["product_id"]
        product_type = req_data["product_type"]
        brand = req_data["brand"]
        date_of_post = req_data["date_of_post"]
        cost_price = req_data["cost_price"]
        list_price = req_data["list_price"]
        rating = req_data["rating"]
        availability = req_data["availability"]
        util.add_products("mens-wear", product_id, product_type, brand, date_of_post, cost_price, list_price, rating, availability)
        return jsonify({"message": "Data Added"}), 201
    else:
        return jsonify(util.get_products("mens-wear", request.args))


@app.route('/womenswear/', methods=['GET', 'POST'])
def womenswear():
    if request.method == "POST":
        req_data = request.get_json()
        product_id = req_data["product_id"]
        product_type = req_data["product_type"]
        brand = req_data["brand"]
        date_of_post = req_data["date_of_post"]
        cost_price = req_data["cost_price"]
        list_price = req_data["list_price"]
        rating = req_data["rating"]
        availability = req_data["availability"]
        util.add_products("womens-wear", product_id, product_type, brand, date_of_post, cost_price, list_price, rating, availability)
        return jsonify({"message": "Data Added"}), 201
    else:
        return jsonify(util.get_products("womens-wear", request.args))


@app.route('/mobiles/', methods=['GET', 'POST'])
def mobiles():
    if request.method == "POST":
        req_data = request.get_json()
        product_id = req_data["product_id"]
        product_type = req_data["product_type"]
        brand = req_data["brand"]
        date_of_post = req_data["date_of_post"]
        cost_price = req_data["cost_price"]
        list_price = req_data["list_price"]
        rating = req_data["rating"]
        availability = req_data["availability"]
        util.add_products("mobiles", product_id, product_type, brand, date_of_post, cost_price, list_price, rating, availability)
        return jsonify({"message": "Data Added"}), 201
    else:
        return jsonify(util.get_products("mobiles", request.args))


if __name__ == '__main__':
    app.run(debug=True)

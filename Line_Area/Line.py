import numpy as np


class Line:
    def __init__(self, x, y, c):
        self.x = x
        self.y = y
        self.c = c

    def get_line(self):
        return self.x, self.y, self.c

    def get_line_eq(self):
        if self.y == 1 and self.x == 1:
            return "y = x + " + str(self.c)
        if self.y == 1:
            return "y = " + str(self.x) + "x + " + str(self.c)
        if self.x == 1:
            return str(self.y) + "y = x + " + str(self.c)
        return str(self.y) + "y = " + str(self.x) + "x + " + str(self.c)

    def get_point(self, p):
        return p, int(p * (self.x + self.c) / self.y)

    def get_points(self, x1, x2):
        p_array = np.array([], dtype=int)
        for p in range(x1, x2 + 1):
            p_array = np.append(p_array, [p, (p * self.x + self.c) / self.y])
        return p_array

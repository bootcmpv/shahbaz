import numpy as np


class LineFunc:

    def __init__(self):
        pass

    def intersect_point(self, l1, l2):
        if l1.x * l2.y == l2.x * l1.y:
            raise ValueError("Lines have the same slope.")
        else:
            a = np.array(((-l1.x, l1.y), (-l2.x, l2.y)))
            b = np.array((l1.c, l2.c))
            ix, iy = np.linalg.solve(a, b)
            return int(ix), int(iy)

    def check_point(self, box, l1, l2, p):
        if self.check_point_left(box, l1, l2, p) or self.check_point_right(box, l1, l2, p):
            return True
        else:
            return False

    def check_point_left(self, box, l1, l2, p):
        tri = [l1.get_point(box.nx), l2.get_point(box.ny), self.intersect_point(l1, l2)]

        a = self.area(tri[0][0], tri[0][1], tri[1][0], tri[1][1], tri[2][0], tri[2][1])
        a1 = self.area(p[0], p[1], tri[1][0], tri[1][1], tri[2][0], tri[2][1])
        a2 = self.area(tri[0][0], tri[0][1], p[0], p[1], tri[2][0], tri[2][1])
        a3 = self.area(tri[0][0], tri[0][1], tri[1][0], tri[1][1], p[0], p[1])

        if a == a1 + a2 + a3:
            return True
        else:
            return False

    def check_point_right(self, box, l1, l2, p):
        tri = [l1.get_point(box.px), l2.get_point(box.py), self.intersect_point(l1, l2)]

        a = self.area(tri[0][0], tri[0][1], tri[1][0], tri[1][1], tri[2][0], tri[2][1])
        a1 = self.area(p[0], p[1], tri[1][0], tri[1][1], tri[2][0], tri[2][1])
        a2 = self.area(tri[0][0], tri[0][1], p[0], p[1], tri[2][0], tri[2][1])
        a3 = self.area(tri[0][0], tri[0][1], tri[1][0], tri[1][1], p[0], p[1])

        if a == a1 + a2 + a3:
            return True
        else:
            return False

    def area(self, x1, y1, x2, y2, x3, y3):
        return abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0)

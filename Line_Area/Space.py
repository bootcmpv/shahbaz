import numpy as np


class Space:
    def __init__(self, min, max):
        self.nx = min
        self.px = max
        self.ny = min
        self.py = max

    def random_points(self, n):
        return np.column_stack((np.random.randint(self.nx, self.px + 1, n), np.random.randint(self.ny, self.py + 1, n)))

from Space import Space
from Line import Line
from LineFunc import LineFunc
import numpy as np


def main():
    l1 = input("Enter equation of 1st line: ").replace(" ", "")
    l2 = input("Enter equation of 2nd line: ").replace(" ", "")
    l1 = Line(int(1 if not l1[l1.index('=') + 1:l1.index('x')] else l1[l1.index('=') + 1:l1.index('x')]),
              int(1 if not l1[:l1.index('y')] else l1[:l1.index('y')]),
              int(1 if not l1[l1.index('+') + 1:] else l1[l1.index('+') + 1:]))
    l2 = Line(int(1 if not l2[l2.index('=') + 1:l2.index('x')] else l2[l2.index('=') + 1:l2.index('x')]),
              int(1 if not l2[:l2.index('y')] else l2[:l2.index('y')]),
              int(1 if not l2[l2.index('+') + 1:] else l2[l2.index('+') + 1:]))
    lf = LineFunc()
    pinls = np.array([], dtype=int)

    print("\nEntered Lines Equations are:")
    print(l1.get_line_eq())
    print(l2.get_line_eq())

    min_value = int(input("\nEnter minimum point of space: "))
    max_value = int(input("Enter maximum point of space: "))
    box = Space(min_value, max_value)
    print("Created space with " + str(min_value) + "x" + str(max_value) + " co-ordinates.")
    rand_points = box.random_points(int(input("\nEnter number of random points to be generated: ")))
    print("Generated " + str(len(rand_points)) + " random points in the created space.")
    for p in rand_points:
        if lf.check_point(box, l1, l2, p):
            pinls = np.append(pinls, p)
    print("\nThe random points which resides in the area of intersection of the two line segments are: ")
    print(pinls)

    meannx = np.mean([p[0] for p in rand_points if lf.check_point_left(box, l1, l2, p)])
    medianny = np.median([p[1] for p in rand_points if lf.check_point_left(box, l1, l2, p)])
    print("\nThe mean of the x co-ordinates from the left area of intersection is: " + str(meannx))
    print("The median of y co-ordinates from the left area of intersection is: " + str(medianny))

    arr = np.array([p for p in rand_points if lf.check_point_right(box, l1, l2, p)])
    min_x = arr[np.lexsort((arr[:, 1], arr[:, 0]))][0]
    max_x = arr[np.lexsort((arr[:, 1], arr[:, 0]))][-1]
    min_y = arr[np.lexsort((arr[:, 0], arr[:, 1]))][0]
    max_y = arr[np.lexsort((arr[:, 0], arr[:, 1]))][-1]
    max_dist = 0
    for x in [min_x, max_x]:
        for y in [min_y, max_y]:
            if max_dist < np.linalg.norm(x - y):
                max_dist = np.linalg.norm(x - y)
                arr = [x, y]
    print("\nFrom the right area of intersection, the points with the maximum distance are:")
    print(arr[0], arr[1])
    return 0


if __name__ == "__main__":
    main()

class File:
    def __init__(self, parent, name, content):
        self.parent = parent
        self.name = name
        self.content = content

    @classmethod
    def init(cls):
        return cls(None, "", "")

    def nano(self, parent, name):
        content = ""
        print("Enter file contents:")
        while True:
            line = input()
            if line:
                content = content + line + "\n"
            else:
                break
        parent.dir["'" + name + "'"] = File(parent, name, content.rstrip())

    def rm(self, directory, name):
        if name == "*.*":
            files = [file for file in directory.dir if isinstance(directory.dir[file], File)]
            for file in files:
                directory.dir.pop(file)
        elif "'" + name + "'" not in directory.dir:
            print("rm: cannot remove '" + name + "': No such file or directory")
        else:
            directory.dir.pop("'" + name + "'")

    def cat(self, directory, name):
        if "'" + name + "'" not in directory.dir:
            print("cat: " + name + ": No such file or directory")
        else:
            print(directory.dir["'" + name + "'"].content)

    def tac(self, directory, name):
        if "'" + name + "'" not in directory.dir:
            print("cat: " + name + ": No such file or directory")
        else:
            print(directory.dir["'" + name + "'"].content[::-1])

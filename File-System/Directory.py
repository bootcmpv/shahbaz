class Directory:
    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.dir = {}

    def pwd(self):
        dir_pointer = self.parent
        path = self.name
        while dir_pointer != "no_parent":
            if dir_pointer.name == "/":
                path = dir_pointer.name + path
            else:
                path = dir_pointer.name + "/" + path
            dir_pointer = dir_pointer.parent
        print(path)

    def mkdir(self, name):
        if name in self.dir:
            print("mkdir: cannot create directory ‘" + name + "’: File exists")
        else:
            self.dir[name] = Directory(self, name)

    def rmdir(self, name):
        if name == "*.*":
            directories = [d for d in self.dir if isinstance(self.dir[d], Directory)]
            for d in directories:
                if not bool(self.dir[d].dir):
                    self.dir.pop(d)
        elif name not in self.dir:
            print("rmdir: failed to remove '" + name + "': No such file or directory")
        elif bool(self.dir[name].dir):
            print("rmdir: failed to remove 'dir': Directory not empty")
        else:
            self.dir.pop(name)

    def rm(self, name):
        if name not in self.dir:
            print("rm: failed to remove '" + name + "': No such file or directory")
        else:
            self.dir.pop(name)

    def cd(self, root, name):
        if name == "/":
            return root
        elif name == "..":
            if self.parent == "no_parent":
                return root
            else:
                return self.parent
        if name not in self.dir:
            print("cd: " + name + ": No such file or directory")
            return self
        else:
            return self.dir[name]

    def ls(self):
        for item in self.dir:
            print(item)

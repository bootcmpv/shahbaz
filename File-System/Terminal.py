from Directory import Directory
from File import File
import sys
import os


class Terminal:
    __commands = {
        'help': "Display help",
        'pwd': "Print the present working directory",
        'mkdir': "Make a new directory",
        'rmdir': "Remove an existing directory",
        'cd': "Change the current working directory to a specified directory",
        'ls': "List all files and folders of the current directory",
        'nano': "File editor to edit files",
        'rm': "Remove an existing file",
        'cat': "Show the contents of a file",
        'tac': "Show the contents of a file in reverse.",
        'exit': "Exit the terminal",
    }

    def __init__(self):
        self.root = Directory("no_parent", "/")
        self.file = File.init()

    def run(self):
        pwd = self.root
        command = ['temp']

        while command[0] != "exit":
            prompt = "[root@localhost " + pwd.name + "] $ "
            command = input(prompt).split()

            if not command:
                command = "temp"
                continue
            elif command[0] not in self.__commands:
                print(command[0] + ": command not found...")
            elif command[0] == "help":
                print("\nCommands Help:\n")
                for com in self.__commands:
                    print(com + ": " + self.__commands[com])
            elif command[0] != "exit":
                if len(command) > 2:
                    temp = pwd if len(command[2].split("/")) < 3 else self.__resolve_path(command[2])
                    command[2] = command[2] if len(command[2].split("/")) < 3 else \
                        list(filter(lambda x: x != "", command[2].split("/")))[-1]
                    if command[0] == "rm" and command[1] == "-rf":
                        temp.rm(command[2])
                elif len(command) > 1:
                    temp = pwd if len(command[1].split("/")) < 3 else self.__resolve_path(command[1])
                    command[1] = command[1] if len(command[1].split("/")) < 3 else \
                        list(filter(lambda x: x != "", command[1].split("/")))[-1]
                    if command[0] == "nano":
                        self.file.nano(temp, command[1])
                    elif command[0] in ["rm", "cat", "tac"]:
                        exec("self.file." + command[0] + "(temp, '" + command[1] + "')")
                    elif command[0] == "cd":
                        pwd = eval("temp." + command[0] + "(self.root, '" + command[1] + "')")
                    else:
                        exec("temp." + command[0] + "('" + command[1] + "')")
                else:
                    exec("pwd." + command[0] + "()")

    def __resolve_path(self, path):
        sys.stdout = open(os.devnull, "w")
        temp = self.root
        directories = list(filter(lambda x: x != "", path.split("/")))
        for d in directories[:-1]:
            temp = temp.cd(self.root, d)
        sys.stdout = sys.__stdout__
        return temp

from Terminal import Terminal


def main():
    ter = Terminal()

    ter.run()
    return 0


if __name__ == '__main__':
    main()
